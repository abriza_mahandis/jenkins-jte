void call(app_env) {

    println "${app_env.short_name}"
    String nodeName = "master"
    node(nodeName){
        stage("Deploy to ${app_env.short_name}"){
            app_env.ip_addr.each{ ip -> 
            println "Deploy to $ip "
            }
        }
    }
}
